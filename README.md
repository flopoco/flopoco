# FloPoCo

FloPoCo is a generator of Floating-Point (but not only) Cores for FPGAs.

webpage: http://flopoco.org

Copyright © INSA-Lyon, HS-Fulda, ENS-Lyon, INRIA, CNRS, UCBL, 2008-2020
All rights reserved
Contact: Florent.de-Dinechin@insa-lyon.fr, Martin.Kumm@cs.hs-fulda.de

FloPoCo is distributed under the [FloPoCo license](doc/FloPoCo-revised-AGPL-license-v1.1.pdf). It is a modified AGPL, so that the code generated by FloPoCo is itself available under LGPL.

The build procedure is shown in the installation guide that you can find [here](https://flopoco.org/flopoco_installation.html).

Developers welcome!

