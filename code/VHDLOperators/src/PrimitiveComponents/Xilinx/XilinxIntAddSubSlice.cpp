// general c++ library for manipulating streams
#include <iostream>
#include <sstream>

/* header of libraries to manipulate multiprecision numbers
   There will be used in the emulate function to manipulate arbitraly large
   entries */
#include "gmp.h"
#include "mpfr.h"

// include the header of the Operator
#include "flopoco/PrimitiveComponents/Xilinx/XilinxIntAddSubSlice.hpp"
#include "flopoco/PrimitiveComponents/Xilinx/Xilinx_LUT6.hpp"
#include "flopoco/PrimitiveComponents/Xilinx/Xilinx_CARRY4.hpp"

using namespace std;

namespace flopoco
{
  XilinxIntAddSubSlice::XilinxIntAddSubSlice(Operator *parentOp, Target *target, int wIn, bool initial, bool fixed, bool dss, const string &prefix) : Operator(parentOp, target)
  {
    setCopyrightString("Marco Kleinlein");

    stringstream name;
    setCombinatorial();
    setShared();

    if (prefix.empty())
    {
      name << "XilinxIntAddSubSlice_" << wIn;
    }
    else
    {
      name << prefix << "_slice" << wIn;
    }

    if (dss)
    {
      name << "_dss";
    }

    if (initial)
    {
      name << "_init";
    }

    setNameWithFreqAndUID(name.str());
    setCombinatorial();
    srcFileName = "XilinxIntAddSubSlice";
    REPORT(DEBUG, "Building" + this->getName());

    if (dss)
    {
      build_with_dss(parentOp, target, wIn, initial);
    }
    else if (fixed)
    {
      build_fixed_sign(parentOp, target, wIn, initial);
    }
    else
    {
      build_normal(parentOp, target, wIn, initial);
    }
  }

  void XilinxIntAddSubSlice::build_normal(Operator *parentOp, Target *target, int wIn, bool initial)
  {
    lut_op carry_pre_o6;
    lut_op carry_pre_o5 = lut_in(2) | lut_in(3);
    lut_op add_o5 =
      (~lut_in(2) & ~lut_in(3) & lut_in(0)) |
      (lut_in(2) & ~lut_in(3) & ~lut_in(0)) |
      (~lut_in(2) & lut_in(3) & lut_in(0));
    lut_op add_o6 =
      (~lut_in(2) & ~lut_in(3) & (lut_in(0) ^ lut_in(1))) |
      (lut_in(2) & ~lut_in(3) & (~lut_in(0) ^ lut_in(1))) |
      (~lut_in(2) & lut_in(3) & (lut_in(0) ^ ~lut_in(1)));
    lut_init carry_pre(carry_pre_o5, carry_pre_o6);
    lut_init adder(add_o5, add_o6);
    addInput("X", wIn);
    addInput("Y", wIn);
    addInput("negX");
    addInput("negY");
    addInput("Cin");
    addOutput("Cout");
    addOutput("R", wIn);
    declare("cc_di", 4);
    declare("cc_s", 4);
    declare("cc_o", 4);
    declare("cc_co", 4);
    declare("lut_o5", wIn);
    declare("lut_o6", wIn);

    if (wIn < 4)
    {
      stringstream fillup;

      fillup << "(3 downto " << (wIn) << " => '0')";

      vhdl << tab << "cc_di" << range(3, wIn) << " <= " << fillup.str() << ";" << std::endl;
      vhdl << tab << "cc_s" << range(3, wIn) << " <= " << fillup.str() << ";" << std::endl;
      vhdl << tab << "cc_di" << range(wIn - 1, 0) << " <= lut_o5;" << std::endl;
      vhdl << tab << "cc_s" << range(wIn - 1, 0) << " <= lut_o6;" << std::endl;
    }
    else
    {
      vhdl << tab << "cc_di" << " <= lut_o5;" << std::endl;
      vhdl << tab << "cc_s" << " <= lut_o6;" << std::endl;
    }

    for (int i = 0; i < wIn; i++)
    {
      stringstream lut_name;
      lut_name << "lut_bit_" << i;

      Xilinx_LUT6_2 *initial_lut = new Xilinx_LUT6_2(this, target);

      if (initial && i == 0)
      {
        initial_lut->setGeneric("init", carry_pre.get_hex(), 64);
      }
      else
      {
        initial_lut->setGeneric("init", adder.get_hex(), 64);
      }

      inPortMap("i1", "X" + of(i));
      inPortMap("i0", "Y" + of(i));
      inPortMap("i3", "negX");
      inPortMap( "i2", "negY");

      if (initial && i == 0)
      {
        inPortMapCst( "i4", "'0'");
      }
      else
      {
        inPortMapCst("i4", "'1'");
      }

      inPortMapCst("i5", "'1'");
      outPortMap("o5", "lut_o5" + of(i));
      outPortMap("o6", "lut_o6" + of(i));
      vhdl << initial_lut->primitiveInstance(lut_name.str());
    }

    Xilinx_CARRY4 *further_cc = new Xilinx_CARRY4(this, target);
    outPortMap("co", "cc_co");
    outPortMap("o", "cc_o");
    inPortMapCst("cyinit", "'0'");
    inPortMap("ci", "Cin");
    inPortMap( "di", "cc_di");
    inPortMap( "s", "cc_s");
    vhdl << further_cc->primitiveInstance("slice_cc");
    vhdl << "Cout <= cc_co" << of(wIn - 1) << ";" << std::endl;
    vhdl << "R <= cc_o" << range(wIn - 1, 0) << ";" << std::endl;
  }

  void XilinxIntAddSubSlice::build_fixed_sign(Operator *parentOp, Target *target, int wIn, bool initial)
  {
    lut_op add_o5 = (~lut_in(2) & ~lut_in(3) & lut_in(0)) |
                    (lut_in(2) & ~lut_in(3) & ~lut_in(0)) |
                    (~lut_in(2) & lut_in(3) & lut_in(0));
    lut_op add_o6 = (~lut_in(2) & ~lut_in(3) & (lut_in(0) ^ lut_in(1))) |
                    (lut_in(2) & ~lut_in(3) & (~lut_in(0) ^ lut_in(1))) |
                    (~lut_in(2) & lut_in(3) & (lut_in(0) ^ ~lut_in(1)));
    lut_init adder(add_o5, add_o6);
    addInput("X", wIn);
    addInput("Y", wIn);
    addInput("negX");
    addInput("negY");
    addInput("Cin");
    addOutput("Cout");
    addOutput("R", wIn);
    declare("cc_di", 4);
    declare("cc_s", 4);
    declare("cc_o", 4);
    declare("cc_co", 4);
    declare("lut_o5", wIn);
    declare("lut_o6", wIn);

    if (wIn < 4)
    {
      stringstream fillup;

      if (4 - wIn == 1)
      {
        fillup << "\"0\"";
      }
      else
      {
        fillup << "(3 downto " << (wIn) << " => '0')";
      }

      vhdl << tab << "cc_di" << range(3, wIn) << " <= " << fillup.str() << ";" << std::endl;
      vhdl << tab << "cc_s" << range(3, wIn) << " <= " << fillup.str() << ";" << std::endl;
      vhdl << tab << "cc_di" << range(wIn - 1, 0) << " <= lut_o5;" << std::endl;
      vhdl << tab << "cc_s" << range(wIn - 1, 0) << " <= lut_o6;" << std::endl;
    }
    else
    {
      vhdl << tab << "cc_di" << " <= lut_o5;" << std::endl;
      vhdl << tab << "cc_s" << " <= lut_o6;" << std::endl;
    }

    for (int i = 0; i < wIn; i++)
    {
      stringstream lut_name;
      lut_name << "lut_bit_" << i;
      Xilinx_LUT6_2 *initial_lut = new Xilinx_LUT6_2(this, target);
      initial_lut->setGeneric("init", adder.get_hex(), 64);
      inPortMap("i1", "X" + of(i));
      inPortMap("i0", "Y" + of(i));
      inPortMap("i3", "negX");
      inPortMap("i2", "negY");
      inPortMapCst("i4", "'1'");
      inPortMapCst("i5", "'1'");
      outPortMap("o5", "lut_o5" + of(i));
      outPortMap("o6", "lut_o6" + of(i));
      vhdl << initial_lut->primitiveInstance(lut_name.str());
    }

    Xilinx_CARRY4 *further_cc = new Xilinx_CARRY4(this, target);
    outPortMap("co", "cc_co");
    outPortMap("o", "cc_o");
    inPortMapCst("cyinit", "'0'");
    inPortMap("ci", "Cin");
    inPortMap("di", "cc_di");
    inPortMap("s", "cc_s");
    vhdl << further_cc->primitiveInstance("slice_cc");
    vhdl << "Cout <= cc_co" << of(wIn - 1) << ";" << std::endl;
    vhdl << "R <= cc_o" << range(wIn - 1, 0) << ";" << std::endl;
  }

  void XilinxIntAddSubSlice::build_with_dss(Operator *parentOp, Target *target, int wIn, bool initial)
  {
    addInput("X", wIn);
    addInput("Y", wIn);
    addInput("negX");
    addInput("negY");
    addInput("Cin");
    addOutput("Cout");
    addOutput("R", wIn);
    addInput("bbus_in", wIn);
    addOutput("bbus_out", wIn);
    declare("cc_di", 4);
    declare("cc_s", 4);
    declare("cc_o", 4);
    declare("cc_co", 4);
    declare("lut_o5", wIn);
    declare("lut_o6", wIn);
    declare("bb_t", wIn);

    if (wIn < 4)
    {
      stringstream fillup;

      if (4 - wIn == 1)
      {
        fillup << "\"0\"";
      }
      else
      {
        fillup << "(3 downto " << (wIn) << " => '0')";
      }

      vhdl << tab << "cc_di" << range(3, wIn) << " <= " << fillup.str() << ";" << std::endl;
      vhdl << tab << "cc_s" << range(3, wIn) << " <= " << fillup.str() << ";" << std::endl;
      vhdl << tab << "cc_di" << range(wIn - 1, 0) << " <= bbus_in;" << std::endl;
      vhdl << tab << "cc_s" << range(wIn - 1, 0) << " <= lut_o6;" << std::endl;
    }
    else
    {
      vhdl << tab << "cc_di" << " <= bbus_in;" << std::endl;
      vhdl << tab << "cc_s" << " <= lut_o6;" << std::endl;
    }

    for (int i = 0; i < wIn; i++)
    {
      stringstream lut_name;
      lut_name << "lut_bit_" << i;
      Xilinx_LUT6_2 *cur_lut = new Xilinx_LUT6_2(this, target);

      if (initial && i == 0)
      {
        cur_lut->setGeneric("init", getLUT_dss_init(), 64);
      }
      else if (initial && i == 1)
      {
        cur_lut->setGeneric("init", getLUT_dss_sec(), 64);
      }
      else
      {
        cur_lut->setGeneric("init", getLUT_dss_std(), 64);
      }

      inPortMap("i0", "Y" + of(i));
      inPortMap("i1", "X" + of(i));
      inPortMap("i2", "negY");
      inPortMap("i3", "negX");
      inPortMap("i4", "bbus_in" + of(i));
      inPortMapCst("i5", "'1'");
      outPortMap("o5", "bb_t" + of(i));
      outPortMap("o6", "lut_o6" + of(i));
      vhdl << cur_lut->primitiveInstance(lut_name.str());
    }

    Xilinx_CARRY4 *further_cc = new Xilinx_CARRY4(this, target);
    outPortMap("co", "cc_co");
    outPortMap("o", "cc_o");
    inPortMapCst("cyinit", "'0'");
    inPortMap("ci", "Cin");
    inPortMap("di", "cc_di");
    inPortMap("s", "cc_s");
    vhdl << further_cc->primitiveInstance("slice_cc");
    vhdl << "Cout <= cc_co" << of(wIn - 1) << ";" << std::endl;
    vhdl << "R <= cc_o" << range(wIn - 1, 0) << ";" << std::endl;
    vhdl << "bbus_out <= bb_t;" << std::endl;
  }

  string XilinxIntAddSubSlice::getLUT_dss_init()
  {
    lut_op fa_s = (lut_in(0) ^ lut_in(2)) ^ (lut_in(1) ^ lut_in(3)) ^ (lut_in(2) ^ lut_in(3));
    lut_op fa_c =
      ((lut_in(0) ^ lut_in(2)) & (lut_in(1) ^ lut_in(3))) |
      ((lut_in(0) ^ lut_in(2)) & (lut_in(2) ^ lut_in(3))) |
      ((lut_in(1) ^ lut_in(3)) & (lut_in(2) ^ lut_in(3)));
    lut_op o5 = fa_c;
    lut_op o6 = fa_s;
    lut_init op(o5, o6);
    return op.get_hex();
  }

  string XilinxIntAddSubSlice::getLUT_dss_sec()
  {
    lut_op fa_s = (lut_in(0) ^ lut_in(2)) ^ (lut_in(1) ^ lut_in(3)) ^ (lut_in(2) & lut_in(3));
    lut_op fa_c =
      ((lut_in(0) ^ lut_in(2)) & (lut_in(1) ^ lut_in(3))) |
      ((lut_in(0) ^ lut_in(2)) & (lut_in(2) & lut_in(3))) |
      ((lut_in(1) ^ lut_in(3)) & (lut_in(2) & lut_in(3)));
    lut_op o5 = fa_c;
    lut_op o6 = fa_s ^ lut_in(4);
    lut_init op(o5, o6);
    return op.get_hex();
  }

  string XilinxIntAddSubSlice::getLUT_dss_std()
  {
    lut_op fa_s = (lut_in(0) ^ lut_in(2)) ^ (lut_in(1) ^ lut_in(3));
    lut_op fa_c = ((lut_in(0) ^ lut_in(2)) & (lut_in(1) ^ lut_in(3)));
    lut_op o5 = fa_c;
    lut_op o6 = fa_s ^ lut_in(4);
    lut_init op(o5, o6);
    return op.get_hex();
  };
}//namespace
