add_hileco_src(
    FixConstant.cpp
    report.cpp
    Target.cpp
    utils.cpp
)

add_subdirectory(ExpLog)
add_subdirectory(FixFunctions)
add_subdirectory(IntMult)
add_subdirectory(SortingNetworks)
add_subdirectory(Tables)
add_subdirectory(Targets)
add_subdirectory(Tools)